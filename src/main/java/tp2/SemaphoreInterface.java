package tp2;

public interface SemaphoreInterface {

	public void up();

	public void down();

	public int releaseAll();
}

